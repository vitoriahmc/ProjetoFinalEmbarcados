#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "asf.h"

//Heart beat
#include "fifo.h"
#include "heartbeat_4md69.h"

/** Reference voltage for AFEC,in mv. */
#define VOLT_REF        (3300)

/** The maximal digital value */
#define MAX_DIGITAL     (4095UL)

#define STRING_EOL    "\n"
#define STRING_HEADER "-- Temperature Sensor --\n\n" \
		"-- "BOARD_NAME" --\n\n" \
		"-- Compiled: "__DATE__" "__TIME__" --"STRING_EOL

#define SENSOR_AFEC         AFEC0
#define SENSOR_AFEC_ID      ID_AFEC0
#define SENSOR_AFEC_CH      AFEC_CHANNEL_5 // Pin PB2
#define SENSOR_AFEC_CH_IR   AFEC_INTERRUPT_EOC_5
#define SENSOR_THRESHOLD    5


//Heart Beat
volatile long g_systimer = 0;

void SysTick_Handler() {
	g_systimer++;
}

/** The conversion data is done flag */
volatile bool is_conversion_done = false;

/** The conversion data value */
volatile uint32_t g_ul_value = 0;


void SENSOR_init(void);

void SENSOR_init(void){
	pmc_enable_periph_clk(SENSOR_AFEC_ID);
};

static void AFEC_Temp_callback(void)
{
	g_ul_value = afec_channel_get_value(SENSOR_AFEC, SENSOR_AFEC_CH);
	is_conversion_done = true;
};

/**
 * \brief Application entry point.
 *
 * \return Unused (ANSI-C compatibility).
 */

static void config_ADC_TEMP(void){
/************************************* 
   * Ativa e configura AFEC
   *************************************/  
  /* Ativa AFEC - 0 */
	afec_enable(AFEC0);

	/* struct de configuracao do AFEC */
	struct afec_config afec_cfg;

	/* Carrega parametros padrao */
	afec_get_config_defaults(&afec_cfg);

	/* Configura AFEC */
	afec_init(AFEC0, &afec_cfg);
  
	/* Configura trigger por software */
	afec_set_trigger(AFEC0, AFEC_TRIG_SW);
  
	/* configura call back */
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_5,	AFEC_Temp_callback, 1); 
   
	/*** Configuracao especfica do canal AFEC ***/
	struct afec_ch_config afec_ch_cfg;
	afec_ch_get_config_defaults(&afec_ch_cfg);
	afec_ch_cfg.gain = AFEC_GAINVALUE_0;
	afec_ch_set_config(AFEC0, SENSOR_AFEC_CH, &afec_ch_cfg);
  
	/*
	* Calibracao:
	* Because the internal ADC offset is 0x200, it should cancel it and shift
	 down to 0.
	 */
	afec_channel_set_analog_offset(AFEC0,SENSOR_AFEC_CH, 0x200);
	
	/* Selecina canal e inicializa converso */  
	afec_channel_enable(AFEC0, SENSOR_AFEC_CH);
		NVIC_SetPriority(AFEC0_IRQn, 10);
	afec_enable_interrupt(AFEC0, SENSOR_AFEC_CH);
	NVIC_EnableIRQ(AFEC0_IRQn);

}

/**
 * \brief Configure UART console.
 * BaudRate : 115200
 * 8 bits
 * 1 stop bit
 * sem paridade  
 
 */

static void configure_console(void)
{

  /* Configura USART1 Pinos */
  sysclk_enable_peripheral_clock(ID_PIOB);
  sysclk_enable_peripheral_clock(ID_PIOA);
  pio_set_peripheral(PIOB, PIO_PERIPH_D, PIO_PB4);  // RX
  pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA21); // TX
 	MATRIX->CCFG_SYSIO |= CCFG_SYSIO_SYSIO4;
 
	const usart_serial_options_t uart_serial_options = {
		.baudrate   = CONF_UART_BAUDRATE,
		.charlength = CONF_UART_CHAR_LENGTH,
		.paritytype = CONF_UART_PARITY,
		.stopbits   = CONF_UART_STOP_BITS,
	};

	/* Configure console UART. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}

/**
 * \brief Configure UART console.
 */
static void USART0_init(void){
  
  /* Configura USART0 Pinos */
  sysclk_enable_peripheral_clock(ID_PIOB);
  pio_set_peripheral(PIOB, PIO_PERIPH_C, PIO_PB0); //Tx	Modulo - Rx Atmel
  pio_set_peripheral(PIOB, PIO_PERIPH_C, PIO_PB1); //Rx Modulo - Tx Atmel
  
  /* Configura opcoes USART */
  const sam_usart_opt_t usart_settings = {
    .baudrate     = 9600,
    .char_length  = US_MR_CHRL_8_BIT,
    .parity_type  = US_MR_PAR_NO,
    .stop_bits    = US_MR_NBSTOP_1_BIT,
    .channel_mode = US_MR_CHMODE_NORMAL
  };

  /* Ativa Clock periferico USART0 */
  sysclk_enable_peripheral_clock(ID_USART0);
  
  /* Configura USART para operar em modo RS232 */
  usart_init_rs232(USART0, &usart_settings, sysclk_get_peripheral_hz());
  
  /* Enable the receiver and transmitter. */
	usart_enable_tx(USART0);
	usart_enable_rx(USART0);
 }

/**
 *  Envia para o UART uma string
 */
uint32_t usart_putString(uint8_t *pstring){
  uint32_t i = 0 ;

  while(*(pstring + i)){
    usart_serial_putchar(USART0, *(pstring+i++));
    while(!uart_is_tx_empty(USART0)){};
  }    
     
  return(i);
}

/*
 * Busca no UART uma string
 */
uint32_t usart_getString(uint8_t *pstring){
  uint32_t i = 0 ;
  
  usart_serial_getchar(USART0, (pstring+i));
  while(*(pstring+i) != '\n'){
    usart_serial_getchar(USART0, (pstring+(++i)));
  }
  *(pstring+i+1)= 0x00;
  return(i);
  
}


int main(void)
{
	uint32_t ul_vol;
	float ul_temp;

	/* Initialize the SAM system. */
	sysclk_init();
	board_init();
	SENSOR_init();
	config_ADC_TEMP();
	h4d69_init();
	h4d69_enable_interrupt();

	configure_console();
	
	pio_configure(PIOC, PIO_OUTPUT_1, 1 << 8, PIO_DEFAULT);
	SysTick_Config(sysclk_get_cpu_hz() / 1000); // 1 ms

	/* Output example information. */
	puts(STRING_HEADER);
	afec_start_software_conversion(AFEC0);
	
	/* buffer para recebimento de dados */
	uint8_t bufferRX[100];
	uint8_t bufferTX[100];

	/* Initialize the SAM system */
	
	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	/* Inicializa com serial com PC*/
	
	/* Configura USART0 para comunicacao com o HM-10 */
	USART0_init();
	
	/* Inicializa funcao de delay */
	delay_init( sysclk_get_cpu_hz());
	
	usart_putString("AT");
	usart_putString("AT");
	usart_putString("AT");
	delay_ms(350);
	usart_putString("AT+RESET");
	delay_ms(350);
	
	usart_putString("AT+NAMEvisanto");
	delay_ms(350);
	
	//Heart Beat
	uint8_t batimentos = 0;
	int counter = 0;
	
	long time_start = g_systimer;

	while (1) {
		
		//Heart Beat Abaixo
		
		h4d69_update();
		
		printf("Has beat == %d\n\n\r",h4d69_has_beat());
		
		if(h4d69_has_beat()) {
			
			pio_clear(PIOC, 1 << 8);
			printf("beat\n\r");
			batimentos++;
			} else {
			pio_set(PIOC, 1 << 8);
		}
		
		if(abs(g_systimer - time_start) > 10000) {
			printf("%d bpm \n\r", batimentos);
			batimentos = batimentos*6;
			sprintf(bufferTX, "%d e %d\n", ul_vol, batimentos);
			usart_putString(bufferTX);
			
			batimentos = 0;
			time_start = g_systimer;
		}
		
		h4d69_convert();
		
		// LM ABAIXO

		if(is_conversion_done == true) {
			
			ul_vol = g_ul_value * VOLT_REF / MAX_DIGITAL;

			ul_temp = ((float)ul_vol/10.0);

			printf("Temperatura atual: %4.1f \n", ul_temp);

			afec_start_software_conversion(AFEC0);
			
			is_conversion_done = false;			
		}
		afec_start_software_conversion(AFEC0);
		//delay_ms(10000);
	}
}
